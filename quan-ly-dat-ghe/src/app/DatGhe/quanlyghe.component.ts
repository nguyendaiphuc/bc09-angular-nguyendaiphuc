import { Component, OnInit, ViewChild } from '@angular/core';
import { DsGheComponent } from './dsghe.component';

@Component({
  selector: 'app-home',
  template: `
  <div >
    <app-ds-ghe></app-ds-ghe>
    <!-- Button trigger modal -->
    <div class="container ">
    <button style="width: 40%;" type="button" class="btn btn-success" data-toggle="modal" data-target="#modelId">
      Thêm Ghế
    </button>

    <!-- Modal -->
    <div class="modal fade" id="modelId" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">Thêm Ghế</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
          </div>
          <div class="modal-body">
            <input type="text" class="form-control mb-2" #tenGhe placeholder="Tên Ghế">
            <input type="text" class="form-control mb-2" #soGhe placeholder="Số Ghế">
            <input type="text" class="form-control mb-2" #giaGhe placeholder="Giá Ghế">
            <input type="text" class="form-control mb-2" #trangThai placeholder="Trạng Thái">
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
            <button type="button" class="btn btn-primary" (click)="themGheParent(tenGhe.value,soGhe.value,giaGhe.value,trangThai.value)">Lưu</button>
          </div>
        </div>
      </div>
    </div>
    </div>
  </div>
  `
})

export class QuanLyDatGheComponent implements OnInit {
  @ViewChild(DsGheComponent) EditGhe:any;


  constructor() { }
   themGheParent(...thamSo:any[]){

    let gheDuocThem ={
      TenGhe: thamSo[0],
      SoGhe: thamSo[1],
      Gia: thamSo[2],
      TrangThai:false
    }
    if(thamSo[3] == "false"){
      gheDuocThem.TrangThai = false;
    }else if(thamSo[3] == "true"){
      gheDuocThem.TrangThai = true;
    }

    this.EditGhe.themGhe(gheDuocThem)

   }
  ngOnInit() {
   }
}
