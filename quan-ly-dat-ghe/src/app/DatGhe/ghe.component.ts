import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-ghe',
  template: `

    <ng-container *ngIf="!itemGhe.TrangThai; else gheDaDuocChon" >
    <button  style="width: 45px;" class="btn btn-secondary mt-3 mr-3 " (click)="datGhe()" [ngClass]="{'btn-success':status}">{{itemGhe.SoGhe}}</button>
    </ng-container>

    <ng-template #gheDaDuocChon >
    <button  style="width: 45px;" class="btn btn-danger mt-3 mr-3 " disabled>{{itemGhe.SoGhe}}</button>
    </ng-template>
 `
})

export class GheComponent implements OnInit {
  status:boolean = false;
  @Input() itemGhe:any ="";
  @Output() emitStatus = new EventEmitter()
  constructor() { }
  datGhe(){

    if(this.status){
      this.status = false;
    }else{
      this.status = true;
    }
    this.emitStatus.emit(this.status);
  }
  ngOnInit() { }
}
