import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { DsGheComponent } from './dsghe.component';
import { GheComponent } from './ghe.component';
import { QuanLyDatGheComponent } from './quanlyghe.component';



@NgModule({
  declarations: [DsGheComponent,GheComponent,QuanLyDatGheComponent],
  imports: [FormsModule,CommonModule],
  exports: [QuanLyDatGheComponent],

})
export class DatGheModule { }
