import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-ds-ghe',
  template: `
  <div class="container p-2 text-center border-warning ">
    <h2 class="text-warning">Đặt Ghế Xe CyberSoft</h2>
    <div class="row ">
    <div class="col-5 ">
      <button class="btn btn-outline-warning w-100" disabled>Tài Xế</button>
         <app-ghe *ngFor="let ghe of DanhSachGhe" [itemGhe]="ghe" (emitStatus)="datGheParent($event,ghe)"></app-ghe>
         <hr/>
         <p>Số Ghế Đã Đặt:{{soGheDaDat}}</p>
      <p>Số Ghế Còn Lại:{{soGheConLai}}</p>
    </div>
    <div class="col-7">
    <h3 class="text-warning">Danh Sách Ghế</h3>
    <div class="w-50 m-auto d-flex" *ngFor="let ghe of dsGheDangDat">
      <p class="text-l w-50">Tên Ghế : {{ghe.TenGhe}}</p>
      <p class=" w-50">Giá : {{ghe.Gia}}</p>
    </div>
    </div>

  </div>
  </div>
  `
})

export class DsGheComponent implements OnInit {
  DanhSachGhe:ARR[] = [{SoGhe:1,TenGhe:"số 1", Gia:100, TrangThai:true},
    {SoGhe:2 ,TenGhe:"số 2", Gia:100, TrangThai:false},
    {SoGhe:3,TenGhe:"số 3", Gia:100, TrangThai:false},
    {SoGhe:4,TenGhe:"số 4", Gia:100, TrangThai:false},
    {SoGhe:5,TenGhe:"số 5", Gia:100, TrangThai:true},
    {SoGhe:6,TenGhe:"số 6", Gia:100, TrangThai:false},
    {SoGhe:7,TenGhe:"số 7", Gia:100, TrangThai:false},
    {SoGhe:8,TenGhe:"số 7", Gia:100, TrangThai:false},
    {SoGhe:9,TenGhe:"số 9", Gia:100, TrangThai:false},
    {SoGhe:10,TenGhe:"số 10", Gia:100, TrangThai:false},
    {SoGhe:11,TenGhe:"số 11", Gia:100, TrangThai:false},
    {SoGhe:12,TenGhe:"số 12", Gia:100, TrangThai:false},
    {SoGhe:13,TenGhe:"số 13", Gia:100, TrangThai:false},
    {SoGhe:14,TenGhe:"số 14", Gia:100, TrangThai:false},
    {SoGhe:15,TenGhe:"số 15", Gia:100, TrangThai:false},
    {SoGhe:16,TenGhe:"số 16", Gia:100, TrangThai:false},
    {SoGhe:17,TenGhe:"số 17", Gia:100, TrangThai:false},
    {SoGhe:18,TenGhe:"số 18", Gia:100, TrangThai:false},
    {SoGhe:19,TenGhe:"số 19", Gia:100, TrangThai:false},
    {SoGhe:20,TenGhe:"số 20", Gia:100, TrangThai:false},
    {SoGhe:21,TenGhe:"số 21", Gia:100, TrangThai:true},
    {SoGhe:22,TenGhe:"số 22", Gia:100, TrangThai:false},
    {SoGhe:23,TenGhe:"số 23", Gia:100, TrangThai:false},
    {SoGhe:24,TenGhe:"số 24", Gia:100, TrangThai:false},
    {SoGhe:25,TenGhe:"số 25", Gia:100, TrangThai:false},
    {SoGhe:26,TenGhe:"số 26", Gia:100, TrangThai:false},
    {SoGhe:27,TenGhe:"số 27", Gia:100, TrangThai:false},
    {SoGhe:28,TenGhe:"số 28", Gia:100, TrangThai:false},
    {SoGhe:29,TenGhe:"số 29", Gia:100, TrangThai:false},
    {SoGhe:30,TenGhe:"số 30", Gia:100, TrangThai:true},
    {SoGhe:31,TenGhe:"số 31", Gia:100, TrangThai:false},
    {SoGhe:32,TenGhe:"số 32", Gia:100, TrangThai:false},
    {SoGhe:33,TenGhe:"số 33", Gia:100, TrangThai:false},
    {SoGhe:34,TenGhe:"số 34", Gia:100, TrangThai:false},
    {SoGhe:35,TenGhe:"số 35", Gia:100, TrangThai:false},];

    soGheDaDat:number = 0;
    soGheConLai:number = 0;
    // dsGheDangDat!:ARR[] ;
    dsGheDangDat:any = [];
  constructor() { }

  datGheParent(event:boolean,ghe:ARR){
    if(event){
      this.soGheDaDat++;
      this.soGheConLai--;
      this.dsGheDangDat.push(ghe);
    }else{
      this.soGheDaDat--;
      this.soGheConLai++;
      for(let index in this.dsGheDangDat){
        if(this.dsGheDangDat[index].SoGhe === ghe.SoGhe){
          this.dsGheDangDat.splice(parseInt(index),1)
        }
      }
    }
  }
  themGhe(gheDuocThem:ARR){
    this.DanhSachGhe.push(gheDuocThem);
  }
  ngOnInit() {
    for(let ghe of this.DanhSachGhe){
      if(!ghe.TrangThai){
        this.soGheConLai++;
      }
    };
  }
}

interface ARR {
  SoGhe:number,
  TenGhe:string,
  Gia:number,
  TrangThai:boolean
};
