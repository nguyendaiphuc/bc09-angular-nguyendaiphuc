import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { DatGheModule } from './DatGhe/datghe.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,DatGheModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
